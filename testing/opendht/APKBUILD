# Contributor: Gavin Henry <ghenry@sentrypeer.org>
# Maintainer: Gavin Henry <ghenry@sentrypeer.org>
pkgname=opendht
pkgver=2.3.5
pkgrel=0
pkgdesc="C++14 Distributed Hash Table implementation"
url="https://github.com/savoirfairelinux/opendht"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	argon2-dev
	asio-dev
	cmake
	gnutls-dev
	msgpack-cxx-dev
	nettle-dev
	readline-dev
	samurai
	"
subpackages="$pkgname-static $pkgname-libs $pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/savoirfairelinux/opendht/archive/refs/tags/$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DOPENDHT_C=ON \
		-DOPENDHT_TOOLS=ON \
		-G Ninja \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cef5a56b93073bcb5ff46caf1ccfdb88ceafbebb58df7c9d45d6e75b3d380d3e5fe8d598566f2a54751b727ec77f1a9272d139422ff1a97f8589f0dfd342d39b  opendht-2.3.5.tar.gz
"
